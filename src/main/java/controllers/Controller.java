package controllers;

import org.apache.log4j.Logger;
import service.AllTaskService;

import java.util.Arrays;

public class Controller {
    private final static Logger logger = Logger.getLogger(String.valueOf(Controller.class));
    private AllTaskService allTaskService = new AllTaskService();

    public void findSimilarWord(){
        allTaskService.createText();
        allTaskService.findMaxSentencesWithSimilarWords();
    }
    public void findWordsInFirstSentencesWhichNotInOther(){
        allTaskService.createText();
        allTaskService.findWordInFirstSentenceWhichIsNotInOtherSentences();
    }
    public void sortBySentence(){
        allTaskService.createText();
        allTaskService.getAllByIncreaseSentence();
    }
    public void questionSentences(){
        allTaskService.createText();
        allTaskService.lengthInput();
        allTaskService.findQuestionsWithInputLength();
    }

    public void increaseByVowels(){
        allTaskService.createText();
        allTaskService.sortByIncreaseVowels();
    }
    public void sortByAlphabet(){
        allTaskService.createText();
        logger.info(allTaskService.sortByAlphabet());
    }
    public void deleteFirstLetterEachWord(){
        allTaskService.createText();
        logger.info(allTaskService.deleteFirstLetterEachWord());
    }
    public void findPalindrome(){
        allTaskService.createText();
        logger.info(allTaskService.findPalindrome());
    }

    public void sortByInputLetter(){
        allTaskService.createText();
        allTaskService.sortByInputLetter();
    }
    public void deleteAllWordsWhichStartWithConsonant(){
        allTaskService.createText();
        allTaskService.deleteAllWordsWhichStartWithConsonant();
    }
    public void readAllFile(){
        allTaskService.createText();
        logger.info(Arrays.toString(allTaskService.readAllFile()));
    }
}
