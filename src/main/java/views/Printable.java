package views;

import java.io.IOException;

public interface Printable {
    void print() throws IOException;
}
