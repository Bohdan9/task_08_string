package views;

import controllers.Controller;
import org.apache.log4j.Logger;

import java.util.*;

public class View {
    private final static Logger logger = Logger.getLogger(String.valueOf(View.class));
    private Locale locale;
    private ResourceBundle resourceBundle;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;

    public View() {
        controller = new Controller();
        input = new Scanner(System.in);
        logger.info("Please chose a language \n" + "\n1) English" + "\n2) Українська \n");
        try {
            int choose = input.nextInt();
            if (choose == 1)
                englishMenu();
            if (choose == 2)
                ukraineMenu();
        } catch (Exception exception) {
            logger.error(Arrays.toString(exception.getStackTrace()));
            logger.error("Invalid variable, please enter from 1 to 2");

        }

    }

    private void putResourceBundle() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));
        menu.put("7", resourceBundle.getString("7"));
        menu.put("8", resourceBundle.getString("8"));
        menu.put("9", resourceBundle.getString("9"));
        menu.put("10", resourceBundle.getString("10"));
        menu.put("11", resourceBundle.getString("11"));
        menu.put("12", resourceBundle.getString("12"));
        menu.put("13", resourceBundle.getString("13"));
        menu.put("14", resourceBundle.getString("14"));
        menu.put("15", resourceBundle.getString("15"));
        menu.put("16", resourceBundle.getString("16"));
        menu.put("17", resourceBundle.getString("17"));
        menu.put("18", resourceBundle.getString("18"));

    }

    private void putMethods() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::largestNumber);
        methodsMenu.put("2", this::increasingSentence);
        methodsMenu.put("3", this::uniqueWords);
        methodsMenu.put("4", this::questionSentences);
        methodsMenu.put("5", this::TEST);
        methodsMenu.put("6", this::sortByAlphabet);
        methodsMenu.put("7", this::sortByIncreaseVowels);
        methodsMenu.put("8", this::TEST);
        methodsMenu.put("9", this::sortByInputLetter);
        methodsMenu.put("10", this::TEST);
        methodsMenu.put("11", this::TEST);
        methodsMenu.put("12", this::deleteAllWordsWhichStartWithConsonant);
        methodsMenu.put("13", this::TEST);
        methodsMenu.put("14", this::findPalindrome);
        methodsMenu.put("15", this::deleteFirstLetterEachWord);
        methodsMenu.put("16", this::TEST);
        methodsMenu.put("17", this::languageMenu);
        methodsMenu.put("18", this::readAllFile);

    }

    private void readAllFile() {
        controller.readAllFile();
    }

    private void deleteAllWordsWhichStartWithConsonant() {
        controller.deleteAllWordsWhichStartWithConsonant();
    }

    private void sortByInputLetter() {
        controller.sortByInputLetter();
    }

    private void findPalindrome() {
        controller.findPalindrome();
    }

    private void deleteFirstLetterEachWord() {
        controller.deleteFirstLetterEachWord();
    }

    private void sortByAlphabet() {
        controller.sortByAlphabet();
    }

    private void sortByIncreaseVowels() {
        controller.increaseByVowels();
    }

    private void questionSentences() {
        controller.questionSentences();
    }

    private void increasingSentence() {
        controller.sortBySentence();
    }


    private void uniqueWords() {
        controller.findWordsInFirstSentencesWhichNotInOther();
    }

    private void englishMenu() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        show();
    }

    private void ukraineMenu() {
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        show();
    }

    private void languageMenu() {
        new View();
    }

    void TEST() {
        logger.info("In progress");

    }

    void largestNumber() {
        controller.findSimilarWord();

    }

    private void outputMenu() {
        logger.info("\n*********************************************       MENU       *********************************************   \n");
        for (String str : menu.values()) {
            logger.info("\n" + str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("\nPlease, select menu point:\n");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
