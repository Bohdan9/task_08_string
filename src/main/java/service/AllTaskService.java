package service;

import models.Sentence;
import models.Text;
import models.Word;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static helper.Constant.FILE_PATH;

public class AllTaskService {
    private final static Logger logger = Logger.getLogger(String.valueOf(AllTaskService.class));
    private static final Set<Integer> VOWELS = "aeioquy".chars().boxed().collect(Collectors.toSet());
    private static Text currentText = new Text(FILE_PATH);
    private Scanner scanner = new Scanner(System.in);
    private int len = 0;

    public AllTaskService() {

    }

    public void createText() {
        try {
            currentText.createText();
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public void findMaxSentencesWithSimilarWords() {
        List<Sentence> sentenceList = currentText.getSentences();
        logger.info(String.format("Number of sentences in a text: %d", sentenceList.size()));
        Map<Word, Set<Sentence>> wordRange = calculateWordRange(sentenceList);
        Word wordWithMaxRange = getWordWithMaxRange(wordRange);
        Set<Sentence> resultSentenceSet = new HashSet<>();
        Set<Sentence> sentenceSetToCheck = new HashSet<>();

        for (Sentence sentence : wordRange.get(wordWithMaxRange)) {
            resultSentenceSet.add(sentence);
            for (Word word : sentence.getWords()) {
                sentenceSetToCheck.addAll(wordRange.get(word));
            }
        }
        sentenceSetToCheck.removeAll(resultSentenceSet);
        checkPotentialSentences(wordRange, resultSentenceSet, sentenceSetToCheck);
        logger.info(String.format("Number of sentences with at least one similar word: %d", resultSentenceSet.size()));

    }

    private void checkPotentialSentences(Map<Word, Set<Sentence>> wordRange, Set<Sentence> resultSentenceSet, Set<Sentence> sentenceSetToCheck) {
        for (Sentence sentenceToCheck : sentenceSetToCheck) {
            boolean isAllOtherContainSimilarWord = true;
            for (Sentence resultSentence : resultSentenceSet) {
                if (!sentenceToCheck.hasSimilarWords(resultSentence, wordRange)) {
                    isAllOtherContainSimilarWord = false;
                    break;
                }
            }
            if (isAllOtherContainSimilarWord) resultSentenceSet.add(sentenceToCheck);
        }
    }

    private Word getWordWithMaxRange(Map<Word, Set<Sentence>> wordRange) {
        Word wordWithMaxRange = null;
        int maxRange = 0;

        for (Word word : wordRange.keySet()) {
            if (wordWithMaxRange == null) {
                wordWithMaxRange = word;
                maxRange = wordRange.get(word).size();
            } else {
                if (maxRange < wordRange.get(word).size()) {
                    wordWithMaxRange = word;
                    maxRange = wordRange.get(word).size();
                } else {
                    if (maxRange == wordRange.get(word).size()) {
                        int countPotentialMaxWordSentence = getMaxSentenceLength(wordRange, word);
                        int countCurrentMaxWordSentence = getMaxSentenceLength(wordRange, wordWithMaxRange);
                        if (countCurrentMaxWordSentence < countPotentialMaxWordSentence) {
                            wordWithMaxRange = word;
                        }
                    }
                }
            }
        }
        return wordWithMaxRange;
    }

    private int getMaxSentenceLength(Map<Word, Set<Sentence>> wordRange, Word wordWithMaxRange) {
        int countCurrentMaxWordSentence = 0;
        for (Sentence sentence : wordRange.get(wordWithMaxRange)) {
            if (countCurrentMaxWordSentence < sentence.getWords().size())
                countCurrentMaxWordSentence = sentence.getWords().size();
        }
        return countCurrentMaxWordSentence;
    }

    private Map<Word, Set<Sentence>> calculateWordRange(List<Sentence> sentenceList) {
        Map<Word, Set<Sentence>> wordRange = new HashMap<>();
        for (Sentence sentence : sentenceList) {
            Set<Word> wordSet = sentence.getWords();
            for (Word word : wordSet) {
                if (wordRange.containsKey(word)) {
                    wordRange.get(word).add(sentence);
                } else {
                    Set<Sentence> sentenceOfWord = new HashSet<>();
                    sentenceOfWord.add(sentence);
                    wordRange.put(word, sentenceOfWord);
                }
            }
        }
        return wordRange;
    }


    public void findWordInFirstSentenceWhichIsNotInOtherSentences() {
        String start = currentText.getWholeText().substring(0, currentText.getWholeText().indexOf("."));
        String end = currentText.getWholeText().substring(currentText.getWholeText().indexOf(".") + 1);
        String[] word = start.replaceAll("[,:()]", "").split(" ");
        ArrayList<String> yesWord = new ArrayList<>();
        ArrayList<String> noWord = new ArrayList<>();
        for (String s : word) {
            if (end.contains(s)) {
                yesWord.add(s);

            } else {
                noWord.add(s);
            }

        }
        logger.info(Arrays.toString(noWord.toArray()));
    }

    public void getAllByIncreaseSentence() {
        char[] ch = currentText.getWholeText().toCharArray();
        StringBuilder builder = new StringBuilder();
        StringBuilder builder1 = new StringBuilder();
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<String> arrayList2 = new ArrayList<>();
        ArrayList<Character> arrayList4 = new ArrayList<>();
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        int countWord = 0;
        int z = 0;
        for (char aCh : ch) {
            if (aCh != ',' && aCh != '.' && aCh != ' ' && aCh != '–' && aCh != ':' && aCh != '?') {
                builder.append(aCh);
            } else {
                arrayList.add(builder.toString());
                builder.setLength(0);
                builder.append(aCh);
                arrayList.add(builder.toString());
                builder.setLength(0);
            }
        }
        for (String s : arrayList) {
            if (!s.equals(".") && !s.equals(" ") && !s.equals("") && !s.equals("–") && !s.equals("?") && !s.equals(":") && !s.equals(",")) {
                builder1.append(s);
                countWord++;
            } else if (s.equals(".") || s.equals("?")) {
                z++;
                builder1.append(s);
                hashMap.put(z, countWord);
                countWord = 0;
                arrayList2.add(builder1.toString());
                builder1.setLength(0);
            } else {
                builder1.append(s);
            }
        }
        List<Map.Entry<Integer, Integer>> list = new ArrayList<>(hashMap.entrySet());
        list.sort(Comparator.comparingInt(Map.Entry::getValue));
        for (Map.Entry<Integer, Integer> o : list) {
            char[] ch4 = o.toString().toCharArray();
            arrayList4.add(ch4[0]);
        }
        for (int i = 0; i < arrayList2.size(); i++) {
            logger.info(arrayList2.get(Character.getNumericValue(arrayList4.get(i)) - 1));
        }
    }

    public void lengthInput() {
        boolean flag = false;
        while (!flag) {
            logger.info("Enter length of words ");
            if (scanner.hasNextInt()) {
                flag = true;
                len = scanner.nextInt();
            } else {
                logger.info("Error!");
                scanner.next();
            }
        }
    }

    public void findQuestionsWithInputLength() {
        StringTokenizer stringTokenizer = new StringTokenizer(currentText.getWholeText(),
                "\n:.!?", true);
        String last;
        String current = "";
        String[] dic = new String[0];
        String[] dicUnique = new String[0];

        while (stringTokenizer.hasMoreTokens()) {
            last = current;
            current = stringTokenizer.nextToken().trim();
            if (current.equals("?")) {
                String[] dic2 = new String[dic.length + 1];
                System.arraycopy(dic, 0, dic2, 0, dic.length);
                dic2[dic.length] = last + current;
                dic = new String[dic2.length];
                System.arraycopy(dic2, 0, dic, 0, dic2.length);
            }
        }
        if (dic.length > 0) {
            logger.info("\nArrays of questions:");
            for (String aDic : dic) {
                logger.info(aDic);
            }
            for (String aDic : dic) {
                StringTokenizer stringTokenizer2 = new StringTokenizer(aDic, " \n\t,.:;!?“”-\"");
                while (stringTokenizer2.hasMoreTokens()) {
                    String tok = stringTokenizer2.nextToken().trim();
                    if (tok.length() == len) {
                        boolean b = false;
                        for (String string : dicUnique) {
                            if (string.equalsIgnoreCase(tok)) {
                                b = true;
                                break;
                            }
                        }
                        if (!b) {
                            String[] dic2 = new String[dicUnique.length + 1];
                            System.arraycopy(dicUnique, 0, dic2, 0, dicUnique.length);
                            dic2[dicUnique.length] = tok;
                            dicUnique = new String[dic2.length];
                            System.arraycopy(dic2, 0, dicUnique, 0, dic2.length);
                        }
                    }
                }
            }
            if (dicUnique.length > 0) {
                System.out.printf("\nArrays of unique words with you length (%d):\n", len);
                for (String aDicUnique : dicUnique) System.out.println(aDicUnique);
            } else
                System.out.printf("\nDon't have words with this length указанной длины (%d)!\n", len);
        } else System.out.printf("\nMiss questions\n", len);
    }


    public void sortByIncreaseVowels() {

        final Set<String> words = Arrays.stream(currentText.getWholeText().split("[\\p{Punct}\\s]+"))
                .map(String::toLowerCase)
                .collect(Collectors.toSet());
        final Map<String, Long> vowelCount = words.stream()
                .collect(Collectors.toMap(Function.identity(), w -> w.chars().filter(VOWELS::contains).count()));
        final List<String> orderedWords = words.stream()
                .sorted((a, b) -> (int) (vowelCount.get(a) - vowelCount.get(b)))
                .collect(Collectors.toList());
        logger.info(orderedWords);
    }

    public List<String> sortByAlphabet() {
        String fullText = currentText.getWholeText();
        String[] allWords = fullText.split(" ");
        return Arrays.stream(allWords).sorted(String::compareTo).collect(Collectors.toList());
    }

    public List<String> deleteFirstLetterEachWord() {
        String read = currentText.getWholeText();
        String[] allWords = read.split(" ");
        List<String> sentencesWithOutFirstLetter = new LinkedList<>();
        for (String s : allWords) {
            sentencesWithOutFirstLetter.add(s.substring(1));
        }
        return sentencesWithOutFirstLetter;
    }

    public String findPalindrome() {
        String read = currentText.getWholeText();
        String[] allWords = read.split(" ");
        List<String> allText = new LinkedList<>();
        for (String s : allWords) {
            StringBuilder reverse = new StringBuilder(s);
            if (s.equals(reverse.reverse().toString())) {
                allText.add(s);
            }
        }
        return allText.stream().sorted((o1, o2) -> o2.length() - o1.length()).collect(Collectors.toList()).get(0);
    }

    public void sortByInputLetter() {
        char input = scanner.next().charAt(0);
        String[] words = currentText.getWholeText().split(" ");
        Arrays.sort(words, new AllTaskService.ByLetterMeetings(input));
        for (String w : words)
            logger.info(w);

    }

    public void deleteAllWordsWhichStartWithConsonant() {
        String read = currentText.getWholeText();
        String[] words = read.split(" ");
        for (String word : words) {
            String[] split = word.split("");
            if (split[0].matches("[bcdfghjklmnprstvwxzBCDFGHJKLMNPRSTVWXZ]")) {
                for (int i = 0; i < words.length; i++) {
                    if (words[i].equals(word)) {
                        words[i] = "";
                    }
                }
            }
        }
    }

    public String[] readAllFile() {
        return currentText.getWholeText().split(" ");
    }

    private static class ByLetterMeetings implements Comparator<String> {
        private char letter;

        ByLetterMeetings(char l) {
            letter = l;
        }

        int matchesCount(String s) {
            int found = 0;
            for (char c : s.toCharArray())
                if (letter == c)
                    ++found;

            return found;
        }

        public int compare(String a, String b) {
            int diff = matchesCount(a) - matchesCount(b);
            return (diff != 0) ? diff : a.compareTo(b);
        }
    }
}
