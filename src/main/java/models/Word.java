package models;

public class Word {
    private String value;

    Word(String string) {
        value = string;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;

        Word word = (Word) o;

        return value.equals(word.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
